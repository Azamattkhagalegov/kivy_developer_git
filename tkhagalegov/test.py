import kivy
from kivy.app import App
from kivy.uix.button import Button

class CalcApp(App):
    def build(self):
        self.root_window.close()
        return Button(text='Click Me!')


if __name__=='__main__':
    CalcApp().run()